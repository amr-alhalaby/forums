<?php
ob_start();
session_start();
include_once '../db/db.php';
if (isset($_SESSION["user_id"])) {
    if (isset($_GET['id'])) {

        $id = $_GET['id'];
        $category_id= $_GET['category_id'];
        $sql = "SELECT active FROM forums where id=$id";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_assoc($result);

        if ($row['active'] == 1) {
            $sql = "UPDATE forums SET active=0 WHERE id=$id";
        } else {

            $sql = "UPDATE forums SET active=1 WHERE id=$id";
        }


        if (mysqli_query($conn, $sql)) {
            header("location: /forum/php/forum/forums.php?id=$category_id");
        } else {
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
} else {
    echo 'login';
}
?>
