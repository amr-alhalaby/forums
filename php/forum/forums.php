<?php
ob_start();
session_start();
include_once '../db/db.php';
include_once '../model.php';
$category_id = $_GET['id'];
$user_id = $_SESSION["user_id"];   //to get from Session user id
$sql = "SELECT * FROM forums where category_id=$category_id";
$result = mysqli_query($conn, $sql);
include_once '../login/header.php';

include '../../html/forum/forums1.html';

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['id'];
        echo "<tr>";
        echo "<td>";
        echo "<a href='../topic/topics.php?id=$id'>" . $row['title'] . "</a>";
        echo "</td>";

        echo "<td style='text-align: center;'>" . $row['description'] . "</td>";


        if ($_SESSION["user_role"] == 1) {
            echo "<td style='text-align: center;'>";
            echo "<a class='btn btn-default  btn-sm' href='edit-forum.php?id=" . $id . "&category_id=$category_id 'role='button'>Edit</a>";
            echo "<a class='btn btn-default  btn-sm' href='delete-forum.php?id=" . $id . "&category_id= $category_id' role='button'>Delete</a>";
            echo "<a class='btn btn-default  btn-sm' href='active-forum.php?id=" . $id . "&category_id= $category_id 'role='button'>" . activeForum($id) . "</a>";
            echo "</td>";
        }
        echo "</tr>";
    }
} else {
    
}
include_once '../../html/forum/forums2.html';

function activeForum($forumId) {
    global $conn;
    $sql = "SELECT active FROM forums where id=$forumId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    if ($row["active"] == 1) {

        return 'Unactivate';
    } else {

        return 'Activate';
    }
}

include_once '../login/footer.php';
?>
