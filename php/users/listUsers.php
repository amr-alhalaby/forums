<?php

ob_start();
session_start();
include_once '../db/db.php';
if ($_SESSION["user_role"] == 1) {

    $sql = 'SELECT * FROM users';
    $result = mysqli_query($conn, $sql);
    $row_num = mysqli_num_rows($result);
    include_once '../login/header.php';

    include '../../html/users/listing.html';

    mysqli_close($conn);
} else {
    include_once '../login/header.php';
    echo '<i><b>only for admin</b></i>';
}
?>