<?php

ob_start();
session_start();
include_once '../db/db.php';

// define variables and set to empty values
$lnameErr = $fnameErr = $genderErr = $countryErr = $unameErr = $pwErr = $emailErr = "";
$lname = $fname = $gender = $country = $uname = $pswd = $email = "";

// if ($_SERVER["REQUEST_METHOD"] == "POST") {
if (isset($_POST["submit"])) {

    if (empty($_POST["fname"])) {
        $fnameErr = "First Name is required";
    } else {
        $fname = test_input($_POST["fname"]);
    }

    if (empty($_POST["lname"])) {
        $lnameErr = "Last Name is required";
    } else {
        $lname = test_input($_POST["lname"]);
    }

    if (empty($_POST["country"])) {
        $countryErr = "Country is required";
    } else {
        $country = test_input($_POST["country"]);
    }

    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
    } else {
        $gender = test_input($_POST["gender"]);
    }

    if (empty($_POST["uname"])) {
        $unameErr = "User Name is required";
    } else {
        $uname = test_input($_POST["uname"]);
    }

    if (empty($_POST["pswd"])) {
        $pwErr = "Password is required";
    } else {
        $pswd = test_input($_POST["pswd"]);
    }

    if (empty($_POST["email"]) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Email is required";
    } else {
        $email = test_input($_POST["email"]);
    }

    if ($lnameErr == "" && $fnameErr == "" && $genderErr == "" && $countryErr == "" && $unameErr == "" && $pwErr == "" && $emailErr == "") {

         include 'addUsers.php';

    }
  include_once '../login/header.php';
    include '../../html/users/registeration.html';
} else {
    include_once '../login/header.php';

    include '../../html/users/registeration.html';
}

function test_input($data) {
    // $data = trim($data);
    // $data = stripslashes($data);
    // $data = htmlspecialchars($data);
    return $data;
}

?>