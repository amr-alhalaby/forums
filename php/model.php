<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'db/db.php';

function sticky($topicId) {
    global $conn;
    $sql = "SELECT sticky FROM topics where id=$topicId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    if ($row["sticky"] == 1) {

        return 'Unstick';
    } else {

        return 'Sticky';
    }
}

function active($topicId) {
    global $conn;
    $sql = "SELECT active FROM topics where id=$topicId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    if ($row["active"] == 1) {

        return 'Unactivate';
    } else {

        return 'Activate';
    }
}

function author($topicId) {
    global $conn;
    $sql = "SELECT user_name FROM users where id=$topicId ";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row["user_name"];
}

function role($userId) {
    global $conn;
    $sql = "SELECT role FROM users where id=$userId ";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row["role"];
}

function comment_user($topicId) {
    global $conn;
    $sql = "SELECT comments.id as id,body,user_name,date,signature,image,user_id FROM comments,users where comments.topic_id=$topicId and users.id=comments.user_id ";

    $result = mysqli_query($conn, $sql);

    return $result;
}

function replays($topicId) {
    global $conn;
    $sql = "SELECT count(*) as count FROM topics,comments where topics.id=$topicId and comments.topic_id=topics.id";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row["count"];
}

function isForumActive($forumId) {
    global $conn;
    $sql = "SELECT active FROM forums where id=$forumId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    if ($row['active'] == 1) {
        return TRUE;
    }
    return FALSE;
}
function categoryName($categoryId) {
    global $conn;
    $sql = "SELECT * FROM categories where id=$categoryId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row["title"];
}
function forumName($forumId) {
    global $conn;
    $sql = "SELECT * FROM forums where id=$forumId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row["title"];
}
function topicName($topicId) {
    global $conn;
    $sql = "SELECT * FROM topics where id=$topicId";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row["subject"];
}
function lock() {
    global $conn;
    $sql = "SELECT status FROM `lock`";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    if ($row["status"] == 1) {

        return 'Unlock';
    } else {

        return 'Lock';
    }
}
