<?php

ob_start();
session_start();
include_once '../db/db.php';

if ($_SESSION["user_role"] == 1) {


    $sql = "SELECT status FROM `lock`";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    if ($row['status'] == 1) {
        $sql = "UPDATE `lock` SET status=0 ";
    } else {

        $sql = "UPDATE `lock` SET status=1 ";
        session_destroy();
    }


    if (mysqli_query($conn, $sql)) {
        //header( "refresh:0;" );
        header("location: /forum/php/login/login.php");
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>