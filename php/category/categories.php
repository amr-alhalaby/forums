<?php
ob_start();
session_start(); 
include_once '../db/db.php';
//to get from Session user id
$sql = "SELECT * FROM categories";
$result = mysqli_query($conn, $sql);
include_once '../login/header.php';
include_once '../../html/category/categories1.html';

if(mysqli_num_rows($result) > 0){
    // output data of each row
    while ($row = mysqli_fetch_assoc($result)) {
     $id=$row['id'];
        echo "<tr>";
        echo "<td>";
        echo "<a href='../forum/forums.php?id=" .$id. "'>" . $row['title'] . "</a>";
        echo "</td>";

        echo "<td style='text-align: center;'>";
        echo $row['description'];
        echo "</td>";

        if ($_SESSION["user_role"] == 1) 
        {
        echo "<td style='text-align: center;'>";
        echo "<a class='btn btn-default  btn-sm' href='edit-category.php?id=" . $id. "'role='button'>Edit</a>";
        echo "<a class='btn btn-default  btn-sm' href='delete-category.php?id=" . $id. "'role='button'>Delete</a>";
        echo "</td>";
        }
         echo "</tr>";
    
} 
}
else {

}
include_once '../../html/category/categories2.html';
include_once '../login/footer.php';

?>
